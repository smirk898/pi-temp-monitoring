#!/bin/bash

count=0;
total=0; 

for i in $( awk '{ print $2; }' ~/Downloads/Temp.txt )
   do 
     total=$(echo $total+$i | bc )
     ((count++))
   done
echo "scale=2; $total / $count" | bc |

cat >> ~/Average.txt &&

date >> ~/Average.txt
